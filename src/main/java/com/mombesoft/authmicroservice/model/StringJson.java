package com.mombesoft.authmicroservice.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@JsonSerialize
@Data
public class StringJson {
    private String value;

    public StringJson() {
    }

    public StringJson(String value) {
        this.value = value;
    }
}
