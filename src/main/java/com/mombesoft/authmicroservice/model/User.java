package com.mombesoft.authmicroservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import lombok.Data;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document(collection = "users")
public class User {
    @Id
    private String _id;
    private String name;

    @Indexed(unique = true)
    private String username;
    @JsonProperty(access = Access.WRITE_ONLY)
    private String password;
    private List<String> roles;
    private boolean status;
    private boolean passReset;
    private String expireOn;

    public User() {
    }

    public User(String name, String username, String password, List<String> roles) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.roles = roles;
        this.status = true;
        this.passReset = true;

        DateTime dateTime  = new DateTime();
        this.expireOn =  dateTime.plusDays(30).toDateTimeISO().toString();
    }
}
