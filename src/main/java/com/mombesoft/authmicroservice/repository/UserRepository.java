package com.mombesoft.authmicroservice.repository;

import com.mombesoft.authmicroservice.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface UserRepository extends MongoRepository<User, String> {

    User findByUsername(String username);
    User findBy_id(String _id);
}
