package com.mombesoft.authmicroservice.controllers;

import com.mombesoft.authmicroservice.model.StringJson;
import com.mombesoft.authmicroservice.model.User;
import com.mombesoft.authmicroservice.repository.UserRepository;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("users")
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping
    public ResponseEntity<List<User>> index() {
        return  new ResponseEntity<>(this.userRepository.findAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<User> store(
            @RequestBody User user
    ) {
        User u = new User(user.getName(), user.getUsername(), user.getPassword(), user.getRoles());
        return  new ResponseEntity<>(this.userRepository.save(u), HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<User> show(
            @PathVariable String id
    ) {
        return  new ResponseEntity<>(this.userRepository.findBy_id(id), HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity<User> update(
            @RequestBody User user
    ) {
        return  new ResponseEntity<>(this.userRepository.save(user), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<StringJson> destroy(
            @PathVariable String id
    ) {
        this.userRepository.delete(this.userRepository.findBy_id(id));
        return  new ResponseEntity<>(new StringJson("Deleted successfully"), HttpStatus.OK);
    }

    @GetMapping("/unlock/{username}/{defaultPassword}/{confirmPassword}")
    public ResponseEntity<User> unlock(
            @PathVariable String username,
            @PathVariable String defaultPassword,
            @PathVariable String confirmPassword
    ) {
        User user = this.userRepository.findByUsername(username);

        if (! new BCryptPasswordEncoder().matches(defaultPassword, user.getPassword()) && !user.isPassReset()) {
            return new  ResponseEntity<>(new User(), HttpStatus.OK);
        }

        user.setPassword(this.passwordEncoder.encode(confirmPassword));
        user.setPassReset(false);
        DateTime dateTime  = new DateTime();
        user.setExpireOn(dateTime.plusDays(30).toDateTimeISO().toString());

        user =  this.userRepository.save(user);

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping("disableUnable/{username}")
    public ResponseEntity<User> disableUnable(
            @PathVariable String username
    ) {
       User user = this.userRepository.findByUsername(username);
       user.setStatus(!user.isPassReset());
       user = this.userRepository.save(user);

       return new ResponseEntity<>(user, HttpStatus.OK);
    }
}
